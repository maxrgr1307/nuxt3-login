import GAuth from 'vue3-google-oauth2'
export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.use(GAuth, {
        clientId: "777273987358-sc4el173btov2duondsptnu5vvb0qq7e.apps.googleusercontent.com",
        scope: "profile email",
        prompt: "select_account",
        fetch_basic_profile: false,
        plugin_name: 'Cliente web 1',
    })
})
