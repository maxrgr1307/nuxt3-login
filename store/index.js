import { defineStore } from 'pinia'
import { useEndpoint } from '~~/composables/endpoint'

export const useAuth = defineStore({
  id: 'auth',
  state: () => ({
    user: {},
    isLogged: false,
    isloading: false,
    errors: '',
    bodyClass: 'bg-[#663399]',
    sideSpaces: 'px-[11%]',
    verificando: "",
    isToast: { isActive: false, isTitulo: '', isNombre: '', isType: '' }
  }),
  actions: {
    // signup user
    userSignup(data) {
      this.isloading = true
      const response = useEndpoint({ method: 'POST', endpoint: 'register', body: data })
      if (response.success) {
        this.isloading = false
        alert(response.message);
      } else {
        this.isloading = false
        alert(response.message);
      }
    },
    async userLogin(data) {

      this.isloading = true
      const res = await useEndpoint({method: 'POST', endpoint:'login', body: data})
      if (res.response.message == "Success") {
        //const confCookies = { httpOnly: true, maxAge: 60 * 60 * 24 * 7 }
        const user = useCookie('User')
        const usertoken = useCookie('Token')
        user.value = res.response.user
        usertoken.value = res.response.token;
        // ----------------------------------
        this.isloading = false
        this.isLogged = true
        this.user = res.response.user
      } else {
        this.isloading = false
        this.errors = res.response.message[0]
      }
    },
    async getUser() {
      const res = await useEndpoint({ endpoint: 'user', type: 'gpNot' })
      if (res.response.message == "Success") {
        this.isLogged = true
      } else {
        this.errors = res.response.message
      }
    },
    async userLogout() {
      const response = await useEndpoint({ method: 'POST', endpoint: 'logout', type: 'gpNot' })
      const user = useCookie('User')
      const token = useCookie('Token')
      user.value = null;
      token.value = null;
      this.isLogged = false
      this.user = {}
    },
    async resetPassUser(data) {
      const uuid = this.user.uuid
      const dataArray = { uuid: uuid, password: data }
      await useEndpoint({ method: 'POST', endpoint: 'resetPassword', body: dataArray, type: 'gpwith' })
    },
    async recoverPassword(data) {
      this.isloading = true
      const res = await useEndpoint({ method: 'POST', endpoint: 'forgot-password', body: data})
      if (res.response.message != "Success") {
        this.errors = res.response.message
      }
      this.isloading = false
    },
    async resetPassword(data) {
      this.isloading = true
      const res = await useEndpoint({ method: 'POST', endpoint: 'reset-password', body: data})
      if (res.response.message != "Success") {
        this.errors = res.response.message
      }
      this.isloading = false
    },


    getCookies() {
      const user = useCookie('User')
      const token = useCookie('Token')
      if (user.value && token.value) {
        this.user = user.value
        this.isLogged = true
      } else {
        //this.userLogout()
        //console.log("%cNo hay credenciales en la cookies", 'font-size: 20px; color: red;')
      }
    }

  }
})

