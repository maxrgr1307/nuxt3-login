import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    ssr: true,
    head: {
        title: 'academia viva',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'hola mundito' },
            { name:'google-site-verification', content: 'bK9N34t5krgg3zLRovEeqi6QAHnWOqItMN-cgvxCMLo' }
        ]
    },
    buildModules: ['@pinia/nuxt'],
    css: [
        '@/assets/css/tailwind.css',
    ],
    build: {
        postcss: {
            postcssOptions: {
                plugins: {
                    tailwindcss: {},
                    autoprefixer: {},
                },
            },
        },
    },
    runtimeConfig: {
        apiSecretGoogle: '777273987358-sc4el173btov2duondsptnu5vvb0qq7e.apps.googleusercontent.com',
        public: {
            apiBase: process.env.PUBLIC_API_BASE || 'https://laravel-api-v1.herokuapp.com/api'
        }
    },
})
